<?php

class News extends MY_Controller
{
    public function index($page = null)
    {
        $data['title'] = 'News';
        $data['news']    = $this->news->paginate($page)->get();
        $data['total_rows'] = $this->news->count();
        $data['pagination'] = $this->news->makePagination(base_url('news'), 2, $data['total_rows']);
        $data['page'] = 'news/news';
        $this->view2($data);
    }
}
