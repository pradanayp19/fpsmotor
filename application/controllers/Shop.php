<?php

class Shop extends MY_Controller
{
	public function index($page = null)
	{
		$data['title'] = 'Shop';
		$data['produk']    = $this->shop->select(
			[
				'product.id', 'product.title AS product_title', 'product.description', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title', 'category.slug AS category_slug'
			]
		)
			->join('category')
			->where('product.is_available', 1)
			->paginate($page)
			->get();

		$data['total_rows'] = $this->shop->where('product.is_available', 1)->count();
		$data['pagination'] = $this->shop->makePagination(base_url('shop'), 2, $data['total_rows']);

		$data['page'] = 'shop/shop';
		$this->view2($data);
	}

	public function search($page = null)
	{
		if (isset($_POST['keyword'])) {
			$this->session->set_userdata('keyword', $this->input->post('keyword'));
		} else {
			redirect(base_url('shop'));
		}

		$keyword = $this->session->userdata('keyword');
		$data['title'] = 'Shop';
		$data['produk'] = $this->shop->select(
			[
				'product.id', 'product.title AS product_title', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title'
			]
		)
			->join('category')
			->paginate($page)
			->like('product.title', $keyword)
			->orlike('category.title', $keyword)
			->get();

		$data['total_rows'] = $this->shop->count();
		$data['pagination'] = $this->shop->makePagination(base_url('shop/search'), 3, $data['total_rows']);


		$data['page'] = 'shop/shop';

		$this->view2($data);
	}

	public function reset()
	{
		$this->session->unset_userdata('keyword');

		redirect(base_url('shop'));
	}
}
