<?php

class Showroom extends MY_Controller
{
	public function index($page = null)
	{
		$data['title'] = 'Shop';
		$data['showroom']    = $this->showroom->select(
			[
				'product.id', 'product.title AS product_title', 'product.description', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title', 'category.slug AS category_slug'
			]
		)
			->join('category')
			->where('product.is_available', 1)
			->paginate($page)
			->get();

		$data['total_rows'] = $this->showroom->where('product.is_available', 1)->count();
		$data['pagination'] = $this->showroom->makePagination(base_url('showroom'), 2, $data['total_rows']);

		$data['page'] = 'showroom/showroom';
		$this->view2($data);
	}
}
