<?php

class Sorting extends MY_Controller
{
    public function sortby($sort, $page = null)
    {
        $data['title'] = 'Belanja';
        $data['produk'] = $this->sorting->select(
            [
                'product.id', 'product.title AS product_title', 'product.description', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title', 'category.slug AS category_slug'
            ]
        )
            ->join('category')
            ->where('product.is_available', 1)
            ->orderBy('product.price', $sort)
            ->paginate($page)
            ->get();

        $data['total_rows'] = $this->sorting->where('product.is_available', 1)->count();
        $data['pagination'] = $this->sorting->makePagination(base_url("sorting/sortby/$sort"), 4, $data['total_rows']);
        $data['page'] = 'shop/shop';

        $this->view2($data);
    }

    public function category($category, $page = null)
    {
        $data['title'] = 'Belanja';
        $data['produk']    = $this->sorting->select(
            [
                'product.id', 'product.title AS product_title', 'product.description', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title', 'category.slug AS category_slug'
            ]
        )
            ->join('category')
            ->where('product.is_available', 1)
            ->where('category.slug', $category)
            ->paginate($page)
            ->get();

        $data['total_rows']     = $this->sorting->where('product.is_available', 1)->where('category.slug', $category)->join('category')->count();
        $data['pagination']     = $this->sorting->makePagination(
            base_url("sorting/category/$category"),
            4,
            $data['total_rows']
        );
        $data['category']   = ucwords(str_replace('-', ' ', $category));
        $data['page']       = 'shop/shop';
        $this->view2($data);
    }
}
