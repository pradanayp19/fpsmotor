<?php 

class Data_showroom extends CI_Controller {
	public function index() {
		$data['showroom'] = $this->Model_showroom->tampil_sr()->result();
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/data_showroom', $data);
		$this->load->view('templatead/footer');
	}

	public function tambah_showroom() {
		$kode_psr					= $this->input->post('kode_psr');
		$nama						= $this->input->post('nama');
		$harga						= $this->input->post('harga');
		$kategori_psr				= $this->input->post('kategori_psr');
		$lokasi						= $this->input->post('lokasi');
		$stok						= $this->input->post('stok');
		$keterangan					= $this->input->post('keterangan');
		$gambar_psr					= $_FILES['gambar_psr']['name'];
		if ($gambar_psr =''){} else{
			$config ['upload_path'] = './upload';
			$config ['allowed_types'] = '*';
			$config ['max_size'] = 2000;

			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('gambar_psr')){
				echo "Gambar gagal di Upload!";
			} else{
				$gambar_psr=$this->upload->data('file_name');
			}

		} 

		$data = array(
			'kode_psr'							=> $kode_psr,
			'nama'								=> $nama,
			'harga'								=> $harga,
			'kategori_psr'						=> $kategori_psr,
			'lokasi'							=> $lokasi,
			'stok'								=> $stok,
			'keterangan'						=> $keterangan,
			'gambar_psr'						=> $gambar_psr
		);

		$this->Model_showroom->tambah_showroom($data, 'tb_showroom');
		redirect('admin/data_showroom/index');

	}

	public function detail($id_showroom)
	{
		$data['showroom'] = $this->Model_showroom->detail_showroom($id_showroom);
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/detail_showroom', $data);
		$this->load->view('templatead/footer');
	}

	public function edit($id)
	{
		$where = array('id_showroom' => $id);
		$data['showroom'] = $this->Model_showroom->edit_showroom($where, 'tb_showroom')->result();
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/edit_showroom', $data);
		$this->load->view('templatead/footer');
	}

	public function update_showroom() 
	{
		$id							= $this->input->post('id_showroom');
		$kode_psr					= $this->input->post('kode_psr');
		$nama						= $this->input->post('nama');
		$harga						= $this->input->post('harga');
		$kategori_psr				= $this->input->post('kategori_psr');
		$lokasi						= $this->input->post('lokasi');
		$stok						= $this->input->post('stok');
		$keterangan					= $this->input->post('keterangan');

		$data = array(
			'kode_psr'							=> $kode_psr,
			'nama'								=> $nama,
			'harga'								=> $harga,
			'kategori_psr'						=> $kategori_psr,
			'lokasi'							=> $lokasi,
			'stok'								=> $stok,
			'keterangan'						=> $keterangan
		);

		$where = array(
			'id_showroom' => $id

		);

		$this->Model_showroom->update_showroom($where, $data, 'tb_showroom');
		redirect('admin/data_showroom/index');

	}

	public function hapus($id){

		$where = array('id_showroom' => $id);
		$this->Model_showroom->hapus_data($where, 'tb_showroom');
		redirect('admin/data_showroom/index');
	}

	
}

?>