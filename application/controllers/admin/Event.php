<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Event extends MY_Controller
{


	public function index($page = null)
	{
		$data['title'] = 'Admin: Event';
		$data['event']    = $this->event->paginate($page)->get();
		$data['total_rows'] = $this->event->count();
		$data['pagination'] = $this->event->makePagination(base_url('admin/event'), 2, $data['total_rows']);
		$data['page'] = 'admin/data_event';
		$this->view($data);
	}

	public function create()
	{
		if (!$_POST) {
			$input = (object) $this->event->getDefaultValues();
		} else {
			$input = (object) $this->input->post(null, true);
		}

		if (!empty($_FILES) && $_FILES['image_event']['name'] !== '') {
			$imageName = url_title($input->title_event, '-', true) . '-' . date('YmdHis');
			$upload    = $this->event->uploadImage('image_event', $imageName);
			if ($upload) {
				$input->image_event   = $upload['file_name'];
			} else {

				redirect(base_url('admin/event/create'));
			}
		}


		if (!$this->event->validate()) {
			$data['title']   = 'Tambah Kategori';
			$data['input']   = $input;
			$data['form_action'] = base_url('admin/event/create');
			$data['page']   = 'admin/form_event';

			$this->view($data);
			return;
		}

		if ($this->event->create($input)) {
			$this->session->set_flashdata('success', 'Data berhasil disimpan!');
		} else {
			$this->session->set_flashdata('error', 'Oops! Terjadi suatu kesalahan');
		}

		redirect(base_url('admin/event'));
	}

	public function edit($id)
	{
		$data['event'] = $this->event->where('id_event', $id)->first();

		if (!$data['event']) {
			$this->session->set_flashdata('warning', 'Data Event Tidak Ditemukan');
			redirect(base_url('admin/event'));
		}

		if (!$_POST) {
			$data['input'] = $data['event'];
		} else {
			$data['input'] = (object) $this->input->post(null, true);
		}

		if (!empty($_FILES) && $_FILES['image_event']['name'] !== '') {
			$imageName = url_title($data['input']->title, '-', true) . '-' . date('YmdHis');
			$upload    = $this->event->uploadImage('image_event', $imageName);
			if ($upload) {
				if ($data['event']->image_event !== '') {
					$this->event->deleteImage($data['event']->image_event);
				}
				$data['input']->image_event   = $upload['file_name'];
			} else {

				redirect(base_url("admin/event/edit/$id"));
			}
		}

		if (!$this->event->validate()) {
			$data['title'] = 'Edit event';
			$data['form_action'] = base_url("admin/event/edit/$id");
			$data['page'] = 'admin/form_event';

			$this->view($data);
			return;
		}

		if ($this->event->where('id_event', $id)->update($data['input'])) {
			$this->session->set_flashdata('success', 'Data berhasil disimpan!');
		} else {
			$this->session->set_flashdata('error', 'Oops! Terjadi sesuatu kesalahan');
		}

		redirect(base_url('admin/event'));
	}

	public function delete($id)
	{
		if (!$_POST) {
			redirect(base_url('admin/event'));
		}

		if (!$this->event->where('id_event', $id)->first()) {
			$this->session->set_flashdata('warning', 'maaf data tidak ditemukan');
			redirect(base_url('admin/event'));
		}

		if ($this->event->where('id_event', $id)->delete()) {
			$this->session->set_flashdata('success', 'Data sudah berhasil dihapus');
		} else {
			$this->session->set_flashdata('error', 'Oops! Terjadi sesuatu kesalahan');
		}

		redirect(base_url(('admin/event')));
	}
}

/* End of file event.php */
