<?php

defined('BASEPATH') or exit('No direct script access allowed');

class News extends MY_Controller
{


    public function index($page = null)
    {
        $data['title'] = 'Admin: News';
        $data['news']    = $this->news->paginate($page)->get();
        $data['total_rows'] = $this->news->count();
        $data['pagination'] = $this->news->makePagination(base_url('admin/news'), 2, $data['total_rows']);
        $data['page'] = 'admin/data_news';
        $this->view($data);
    }

    public function create()
    {
        if (!$_POST) {
            $input = (object) $this->news->getDefaultValues();
        } else {
            $input = (object) $this->input->post(null, true);
        }

        if (!empty($_FILES) && $_FILES['image_news']['name'] !== '') {
            $imageName = url_title($input->title_news, '-', true) . '-' . date('YmdHis');
            $upload    = $this->news->uploadImage('image_news', $imageName);
            if ($upload) {
                $input->image_news   = $upload['file_name'];
            } else {

                redirect(base_url('admin/news/create'));
            }
        }


        if (!$this->news->validate()) {
            $data['title']   = 'Tambah Kategori';
            $data['input']   = $input;
            $data['form_action'] = base_url('admin/news/create');
            $data['page']   = 'admin/form_news';

            $this->view($data);
            return;
        }

        if ($this->news->create($input)) {
            $this->session->set_flashdata('success', 'Data berhasil disimpan!');
        } else {
            $this->session->set_flashdata('error', 'Oops! Terjadi suatu kesalahan');
        }

        redirect(base_url('admin/news'));
    }

    public function edit($id)
    {
        $data['news'] = $this->news->where('id_news', $id)->first();

        if (!$data['news']) {
            $this->session->set_flashdata('warning', 'Data news Tidak Ditemukan');
            redirect(base_url('admin/news'));
        }

        if (!$_POST) {
            $data['input'] = $data['news'];
        } else {
            $data['input'] = (object) $this->input->post(null, true);
        }

        if (!empty($_FILES) && $_FILES['image_news']['name'] !== '') {
            $imageName = url_title($data['input']->title, '-', true) . '-' . date('YmdHis');
            $upload    = $this->news->uploadImage('image_news', $imageName);
            if ($upload) {
                if ($data['news']->image_news !== '') {
                    $this->news->deleteImage($data['news']->image_news);
                }
                $data['input']->image_news   = $upload['file_name'];
            } else {

                redirect(base_url("admin/news/edit/$id"));
            }
        }

        if (!$this->news->validate()) {
            $data['title'] = 'Edit news';
            $data['form_action'] = base_url("admin/news/edit/$id");
            $data['page'] = 'admin/form_news';

            $this->view($data);
            return;
        }

        if ($this->news->where('id_news', $id)->update($data['input'])) {
            $this->session->set_flashdata('success', 'Data berhasil disimpan!');
        } else {
            $this->session->set_flashdata('error', 'Oops! Terjadi sesuatu kesalahan');
        }

        redirect(base_url('admin/news'));
    }

    public function delete($id)
    {
        if (!$_POST) {
            redirect(base_url('admin/news'));
        }

        if (!$this->news->where('id_news', $id)->first()) {
            $this->session->set_flashdata('warning', 'maaf data tidak ditemukan');
            redirect(base_url('admin/news'));
        }

        if ($this->news->where('id_news', $id)->delete()) {
            $this->session->set_flashdata('success', 'Data sudah berhasil dihapus');
        } else {
            $this->session->set_flashdata('error', 'Oops! Terjadi sesuatu kesalahan');
        }

        redirect(base_url(('admin/news')));
    }
}

/* End of file event.php */
