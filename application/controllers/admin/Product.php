<?php

class Product extends MY_Controller
{
	public function index($page = null)
	{
		$data['title']      = 'Admin: Category';
		$data['produk']		= $this->product->select(
			[
				'product.id', 'product.title AS product_title', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title'
			]
		)
			->join('category')
			->paginate($page)
			->get();

		$data['total_rows'] = $this->product->count();
		$data['pagination'] = $this->product->makePagination(base_url('admin/product'), 2, $data['total_rows']);
		$data['page']       = 'admin/data_produk';

		$this->view($data);
	}

	public function search($page = null)
	{
		if (isset($_POST['keyword'])) {
			$this->session->set_userdata('keyword', $this->input->post('keyword'));
		} else {
			redirect(base_url('admin/product'));
		}

		$keyword = $this->session->userdata('keyword');
		$data['title'] = 'Admin: Product';
		$data['produk'] = $this->product->select(
			[
				'product.id', 'product.title AS product_title', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title'
			]
		)
			->join('category')
			->paginate($page)
			->like('product.title', $keyword)
			->orlike('category.title', $keyword)
			->get();
		$data['total_rows'] = $this->product->count();
		$data['pagination'] = $this->product->makePagination(base_url('admin/product/search'), 3, $data['total_rows']);


		$data['page'] = 'admin/data_produk';

		$this->view($data);
	}

	public function reset()
	{
		$this->session->unset_userdata('keyword');

		redirect(base_url('admin/product'));
	}

	public function create()
	{
		if (!$_POST) {
			$input = (object) $this->product->getDefaultValues();
		} else {
			$input = (object) $this->input->post(null, true);
		}

		if (!empty($_FILES) && $_FILES['image']['name'] !== '') {
			$imageName = url_title($input->title, '-', true) . '-' . date('YmdHis');
			$upload    = $this->product->uploadImage('image', $imageName);
			if ($upload) {
				$input->image   = $upload['file_name'];
			} else {

				redirect(base_url('admin/product/create'));
			}
		}

		if (!$this->product->validate()) {
			$data['title']          = 'Tambah Produk';
			$data['input']          = $input;
			$data['form_action']    = base_url('admin/product/create');
			$data['page']           = 'admin/form_product';

			$this->view($data);
			return;
		}

		if ($this->product->create($input)) {
			$this->session->set_flashdata('success', 'Data berhasil disimpan!');
		} else {
			$this->session->set_flashdata('error', 'Oops! Terjadi suatu kesalahan.');
		}

		redirect(base_url('admin/product'));
	}

	public function edit($id)
	{
		$data['produk'] = $this->product->where('id', $id)->first();

		if (!$data['produk']) {
			$this->session->set_flashdata('warning', 'Data tidak dapat ditemukan');
			redirect(base_url('admin/product'));
		}

		if (!$_POST) {
			$data['input'] = $data['produk'];
		} else {
			$data['input'] = (object) $this->input->post(null, true);
		}

		if (!empty($_FILES) && $_FILES['image']['name'] !== '') {
			$imageName = url_title($data['input']->title, '-', true) . '-' . date('YmdHis');
			$upload    = $this->product->uploadImage('image', $imageName);
			if ($upload) {
				if ($data['produk']->image !== '') {
					$this->product->deleteImage($data['produk']->image);
				}
				$data['input']->image   = $upload['file_name'];
			} else {

				redirect(base_url("admin/product/edit/$id"));
			}
		}

		if (!$this->product->validate()) {
			$data['title'] = 'Ubah Produk';
			$data['form_action'] = base_url("admin/product/edit/$id");
			$data['page'] = 'admin/form_product';

			$this->view($data);
			return;
		}

		if ($this->product->where('id', $id)->update($data['input'])) {
			$this->session->set_flashdata('success', 'Data berhasil diubah');
		} else {
			$this->session->set_flashdata('error', 'Oops! Terjadi suatu kesalahan');
		}

		redirect(base_url('admin/product'));
	}

	public function delete($id)
	{
		if (!$_POST) {
			redirect(base_url('admin/product'));
		}

		if (!$this->product->where('id', $id)->first()) {
			$this->session->set_flashdata('warning', 'maaf data tidak ditemukan');
			redirect(base_url('admin/product'));
		}

		if ($this->product->where('id', $id)->delete()) {
			$this->session->set_flashdata('success', 'Data sudah berhasil dihapus');
		} else {
			$this->session->set_flashdata('error', 'Oops! Terjadi sesuatu kesalahan');
		}

		redirect(base_url(('admin/product')));
	}
}
