<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Event_model extends MY_Model
{

    protected $table    = 'event';
    protected $perPage  = 1;

    public function getDefaultValues()
    {
        return [
            'id_event'    => '',
            'title_event'  => '',
            'category' => '',
            'description' => '',
            'date_event' => '',
            'image_event' => '',
            'admin' => ''
        ];
    }

    public function getValidationRules()
    {
        $validationRules = [
            [
                'field' => 'title_event',
                'label' => 'Nama Event',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'category',
                'label' => 'Kategori',
                'rules'  => 'trim|required'
            ],
            [
                'field' => 'description',
                'label' => 'Deskripsi',
                'rules'  => 'trim|required'
            ],
            [
                'field' => 'date_event',
                'label' => 'Tanggal Event',
                'rules'  => 'trim|required'
            ],
            [
                'field' => 'admin',
                'label' => 'Admin Posting',
                'rules'  => 'trim|required'
            ],

        ];

        return $validationRules;
    }

    public function uploadImage($fieldName, $fileName)
    {
        $config = [
            'upload_path'       => './images/event',
            'file_name'         => $fileName,
            'allowed_types'     => 'jpg|gif|png|jpeg|JPG|PNG',
            'max_size'          => '2048',
            'max_width'         => 0,
            'max_height'        => 0,
            'overwrite'         => true,
            'file_ext_tolower'  => true
        ];

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($fieldName)) {
            return $this->upload->data();
        } else {
            $this->session->set_flashdata('image_error', $this->upload->display_errors('', ''));
            return false;
        }
    }



    public function deleteImage($fileName)
    {
        if (file_exists("./images/event/$fileName")) {
            unlink("./images/event/$fileName");
        }
    }
}


/* End of file Event_model.php */