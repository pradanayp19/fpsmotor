<?php

class Model_event extends CI_Model {
	public function tampil_eve() 
	{
		return $this->db->get('tb_event');
	}

	public function tambah_event($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_event($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_event($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function find($id){
		$result = $this->db->where('id_event', $id)
		->limit(1)
		->get('tb_event');

		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}


	}

	public function detail_event($id_event)
	{
		$result = $this->db->where('id_event', $id_event)->get('tb_event');
		if($result->num_rows() > 0){
			return $result->result();
		} else {
			return false;
		}
	}
}