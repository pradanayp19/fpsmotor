<?php

class Model_showroom extends CI_Model {
	public function tampil_sr() 
	{
		return $this->db->get('tb_showroom');
	}

	public function tambah_showroom($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_showroom($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_showroom($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function find($id){
		$result = $this->db->where('id_showroom', $id)
		->limit(1)
		->get('tb_showroom');

		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}


	}

	public function detail_showroom($id_showroom)
	{
		$result = $this->db->where('id_showroom', $id_showroom)->get('tb_showroom');
		if($result->num_rows() > 0){
			return $result->result();
		} else {
			return false;
		}
	}
}