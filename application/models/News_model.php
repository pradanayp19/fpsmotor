<?php

defined('BASEPATH') or exit('No direct script access allowed');

class News_model extends MY_Model
{

	protected $table    = 'news';
	protected $perPage  = 2;

	public function getDefaultValues()
	{
		return [
			'id'    => '',
			'title_news'  => '',
			'category' => '',
			'description' => '',
			'date_news' => '',
			'image_news' => '',
			'admin' => ''
		];
	}

	public function getValidationRules()
	{
		$validationRules = [
			[
				'field' => 'title_news',
				'label' => 'Nama News',
				'rules' => 'trim|required'
			],
			[
				'field' => 'category',
				'label' => 'Kategori',
				'rules'  => 'trim|required'
			],
			[
				'field' => 'description',
				'label' => 'Deskripsi',
				'rules'  => 'trim|required'
			],
			[
				'field' => 'date_news',
				'label' => 'Tanggal News',
				'rules'  => 'trim|required'
			],
			[
				'field' => 'admin',
				'label' => 'Admin Posting',
				'rules'  => 'trim|required'
			],

		];

		return $validationRules;
	}

	public function uploadImage($fieldName, $fileName)
	{
		$config = [
			'upload_path'       => './images/news',
			'file_name'         => $fileName,
			'allowed_types'     => 'jpg|gif|png|jpeg|JPG|PNG',
			'max_size'          => '2048',
			'max_width'         => 0,
			'max_height'        => 0,
			'overwrite'         => true,
			'file_ext_tolower'  => true
		];

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($fieldName)) {
			return $this->upload->data();
		} else {
			$this->session->set_flashdata('image_error', $this->upload->display_errors('', ''));
			return false;
		}
	}



	public function deleteImage($fileName)
	{
		if (file_exists("./images/news/$fileName")) {
			unlink("./images/news/$fileName");
		}
	}
}


/* End of file News_model.php */