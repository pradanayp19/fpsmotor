<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Showroom_model extends MY_Model
{

    protected $table    = 'product';
    protected $perPage  = 8;
}

/* End of file Showroom_model.php */