<?php
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Produk</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data Produk</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Produk</h3>
                        </div>

                        <div class="card-body">

                            <a href="<?= base_url('admin/category/create') ?> " class="btn btn-primary mb-3"><i class="fas fa-folder-plus"></i> Tambah</a>

                            <div class="float-right">
                                <?= form_open(base_url('admin/category/search'), ['method' => 'POST']) ?>
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control form-control-sm text-center" placeholder="Cari" value="<?= $this->session->userdata('keyword') ?>">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary btn-sm">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <a href="<?= base_url('admin/category/reset') ?>" class="btn btn-secondary btn-sm">
                                            <i class="fas fa-eraser"></i>
                                        </a>
                                    </div>
                                </div>
                                <?= form_close() ?>
                            </div>

                            <?php $this->load->view('templatead/_alert') ?>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th colspan="3">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Bagian LOOP PRODUK -->
                                    <?php
                                    $no = 1;
                                    foreach ($content as $row) : ?>

                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row->title ?></td>
                                            <td><?php echo $row->slug ?></td>
                                            <td>
                                                <?= form_open("admin/category/delete/$row->id", ['method' => 'POST']) ?>
                                                <?= form_hidden('id', $row->id) ?>
                                                <a href="<?= base_url("admin/category/edit/$row->id") ?>">
                                                    <i class="fas fa-edit text-info"></i>

                                                </a>
                                                <button type="submit" class="btn btn-sm" onclick="return confirm('yakin hapus?')">
                                                    <i class="fas fa-trash text-danger"></i>
                                                </button>
                                                <?= form_close() ?>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                </tbody>

                            </table>

                            <nav aria-label="Page navigation example">
                                <?= $pagination ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>