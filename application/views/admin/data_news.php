<?php
?>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Event</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Data Event</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Event</h3>
            </div>
            <div class="card-body">
              <a href="<?= base_url('admin/news/create') ?> " class="btn btn-primary mb-3"><i class="fas fa-folder-plus"></i> Tambah</a>

              <?php $this->load->view('templatead/_alert') ?>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Event</th>
                    <th>Kategori</th>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th>Admin</th>
                    <th colspan="3">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- Bagian LOOP PRODUK -->
                  <?php
                  $no = 1;
                  foreach ($news as $n) : ?>

                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $n->title_news ?></td>
                      <td><?php echo $n->category ?></td>
                      <td><?php echo $n->date_news ?></td>
                      <td><?php echo $n->description ?></td>
                      <td><?php echo $n->admin ?></td>
                      <td>
                        <?= form_open("admin/news/delete/$n->id", ['method' => 'POST']) ?>
                        <?= form_hidden('id', $n->id) ?>
                        <a href="<?= base_url("admin/news/edit/$n->id") ?>">
                          <i class="fas fa-edit text-info"></i>

                        </a>
                        <button type="submit" class="btn btn-sm" onclick="return confirm('yakin hapus?')">
                          <i class="fas fa-trash text-danger"></i>
                        </button>
                        <?= form_close() ?>
                      </td>
                    </tr>

                  <?php endforeach; ?>
                </tbody>

              </table>
              <nav aria-label="Page navigation example">
                <?= $pagination ?>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>