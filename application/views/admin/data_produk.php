<?php
?>
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Produk</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Data Produk</li>
					</ol>
				</div>
			</div>
		</div>
	</section>

	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Produk</h3>
						</div>

						<div class="card-body">

							<a href="<?= base_url('admin/product/create') ?> " class="btn btn-primary mb-3"><i class="fas fa-folder-plus"></i> Tambah</a>
							<div class="float-right">
								<?= form_open(base_url('admin/product/search'), ['method' => 'POST']) ?>
								<div class="input-group">
									<input type="text" name="keyword" class="form-control form-control-sm text-center" placeholder="Cari" value="<?= $this->session->userdata('keyword') ?>">
									<div class="input-group-append">
										<button class="btn btn-secondary btn-sm">
											<i class="fas fa-search"></i>
										</button>
										<a href="<?= base_url('admin/product/reset') ?>" class="btn btn-secondary btn-sm">
											<i class="fas fa-eraser"></i>
										</a>
									</div>
								</div>
								<?= form_close() ?>
							</div>

							<?php $this->load->view('templatead/_alert') ?>
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Produk</th>
										<th>Kategori</th>
										<th>Harga</th>
										<th>Stok</th>
										<th colspan="3">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<!-- Bagian LOOP PRODUK -->
									<?php
									$no = 1;
									foreach ($produk as $prod) : ?>

										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo $prod->product_title ?></td>
											<td><?php echo $prod->category_title ?></td>
											<td><?php echo $prod->price ?></td>
											<td><?php echo $prod->is_available ? 'Tersedia' : 'Kosong' ?></td>
											<td>
												<?= form_open(base_url("admin/product/delete/$prod->id"), ['method' => 'POST']) ?>
												<?= form_hidden('id', $prod->id) ?>
												<a href="<?= base_url("admin/product/edit/$prod->id") ?>">
													<i class="fas fa-edit text-info"></i>

												</a>
												<button type="submit" class="btn btn-sm" onclick="return confirm('yakin hapus?')">
													<i class="fas fa-trash text-danger"></i>
												</button>
												<?= form_close() ?>
											</td>
										</tr>

									<?php endforeach; ?>
								</tbody>

							</table>

							<nav aria-label="Page navigation example">
								<?= $pagination ?>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>