<?php
?>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Testimoni</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Data Testimoni</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Testimoni</h3>
            </div>
            <div class="card-body">

              <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#tambah_testi"><i class="fas fa-folder-plus"></i>&nbsp Tambah testi</button>&nbsp;
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Profesi</th>
                    <th>Komentar</th>
                    <th colspan="3">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- Bagian LOOP PRODUK -->
                  <?php 
                  $no=1;
                  foreach ($testi as $test) : ?>

                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $test->nama ?></td>
                      <td><?php echo $test->profesi ?></td>
                      <td><?php echo $test->komentar_testi ?></td>
                      <td><?php echo anchor('admin/data_testi/detail/' .$test->id_testi, '<div class="btn btn-success btn-sm"><i class="fa fa-search-plus"></i></div>') ?></td>
                      <td><?php echo anchor('admin/data_testi/edit/' .$test->id_testi, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div>') ?></td>
                      <td><?php echo anchor('admin/data_testi/hapus/' .$test->id_testi, '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>') ?></td>

                    </tr>

                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Profesi</th>
                    <th>Komentar</th>
                    <th colspan="3">Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div id="tambah_testi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0" id="myModalLabel">Isi Data testi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url() . 'admin/data_testi/tambah_testi'; ?>" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" required>
              </div>

              <div class="form-group">
                <label>Profesi</label>
                <input type="text" name="profesi" class="form-control" required>
              </div>

              <div class="form-group">
                <label>Komentar</label>
                <input type="text" name="komentar_testi" class="form-control" required>
              </div>

            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <label>Untuk <code>File Gambar</code></label>
              <hr style="height: 2px;background-color:black;">
              <div class="form-group">
                <label>file .JPG, .PNG</label>
                <input type="file" name="gambar_testi" class="form-control">
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
