<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Edit Data Produk</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Data Produk</a></li>
						<li class="breadcrumb-item active">Edit Data Produk</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card m-b-30 mt-3">
						<div class="card-body">
							<?php foreach ($produk as $prod) : ?>

								<form action="<?= base_url() . 'admin/data_produk/update_produk' ?>" method="post" enctype="multipart/form-data">

									<div class="form-group">
										<label>Kode</label>
										<input type="text" name="kode" class="form-control" value="<?= $prod->kode ?>" min="0">
									</div>

									<div class="form-group">
										<label>Nama Produk</label>
										<input type="hidden" name="id_produk" class="form-control" value="<?= $prod->id_produk ?>">
										<input type="text" name="nama" class="form-control" value="<?= $prod->nama ?>">
									</div>

									<div class="form-group">
										<label>Keterangan</label>
										<textarea id="summernote" type="text" name="keterangan" class="form-control" required><?= $prod->keterangan ?></textarea>
									</div>

									<div class="form-group">
										<label>Kategori</label>
										<select class="form-control" name="kategori" value="<?= $prod->kategori ?>" required>
											<option value="<?= $prod->kategori ?>">- Pilih Kategori -</option>
											<option value="YGP">YGP</option>
											<option value="APPAREL">APPAREL</option>
											<option value="HELMET">HELMET</option>
											<option value="AKSESORIS">AKSESORIS</option>
										</select>
									</div>

									<div class="form-group">
										<label>Harga</label>
										<input type="number" name="harga" id="harga" class="form-control" value="<?= $prod->harga ?>" min="0">
										<p id="harga"></p>
									</div>

									<div class="form-group">
										<label>Stok</label>
										<input type="number" name="stok" class="form-control" value="<?= $prod->stok ?>" min="0">
									</div>

									<div class="form-group">
										<label>Lokasi</label>
										<input type="text" name="lokasi" class="form-control" value="<?= $prod->lokasi ?>" min="0">
									</div>

									<button type="submit" class="btn btn-primary waves-effect waves-light mt-3 col-12">Simpan</button>
								</form>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>