<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Edit Data Showroom</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Data Showroom</a></li>
						<li class="breadcrumb-item active">Edit Data Showroom</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card m-b-30 mt-3">
						<div class="card-body">
							<?php foreach ($showroom as $show) : ?>

								<form action="<?= base_url() . 'admin/data_showroom/update_showroom' ?>" method="post" enctype="multipart/form-data">

									<div class="form-group">
										<label>Kode</label>
										<input type="text" name="kode_psr" class="form-control" value="<?= $show->kode_psr ?>" min="0">
									</div>

									<div class="form-group">
										<label>Nama Produk</label>
										<input type="hidden" name="id_showroom" class="form-control" value="<?= $show->id_showroom ?>">
										<input type="text" name="nama" class="form-control" value="<?= $show->nama ?>">
									</div>

									<div class="form-group">
										<label>Keterangan</label>
										<textarea id="summernote" type="text" name="keterangan" class="form-control" required><?= $show->keterangan ?></textarea>
									</div>

									<div class="form-group">
										<label>Kategori</label>
										<select class="form-control" name="kategori_psr" value="<?= $show->kategori_psr ?>" required>
											<option value="<?= $show->kategori_psr ?>">- Pilih Kategori -</option>
											<option value="YGP">YGP</option>
											<option value="APPAREL">APPAREL</option>
											<option value="HELMET">HELMET</option>
											<option value="AKSESORIS">AKSESORIS</option>
										</select>
									</div>

									<div class="form-group">
										<label>Harga</label>
										<input type="number" name="harga" id="harga" class="form-control" value="<?= $show->harga ?>" min="0">
										<p id="harga"></p>
									</div>

									<div class="form-group">
										<label>Stok</label>
										<input type="number" name="stok" class="form-control" value="<?= $show->stok ?>" min="0">
									</div>

									<div class="form-group">
										<label>Lokasi</label>
										<input type="text" name="lokasi" class="form-control" value="<?= $show->lokasi ?>" min="0">
									</div>

									<button type="submit" class="btn btn-primary waves-effect waves-light mt-3 col-12">Simpan</button>
								</form>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>