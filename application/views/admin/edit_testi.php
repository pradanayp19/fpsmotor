<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Edit Data testi</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Data testi</a></li>
						<li class="breadcrumb-item active">Edit Data testi</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card m-b-30 mt-3">
						<div class="card-body">
							<?php foreach ($testi as $test) : ?>

								<form action="<?= base_url() . 'admin/data_testi/update_testi' ?>" method="post" enctype="multipart/form-data">

									<div class="form-group">
										<label>Nama testi</label>
										<input type="hidden" name="id_testi" class="form-control" value="<?= $test->id_testi ?>">
										<input type="text" name="nama" class="form-control" value="<?= $test->nama_testi ?>" min="0">
									</div>

									<div class="form-group">
										<label>Admin</label>
										<input type="text" name="admin" class="form-control" value="<?= $test->admin ?>">
									</div>

									<div class="form-group">
										<label>Tanggal</label>
										<input type="text" name="tgl_testi" class="form-control" value="<?= $test->tgl_testi ?>">
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Kategori</label>
											<select class="form-control" name="kategori" value="<?= $test->kategori ?>"required>
												<option>- Pilih Kategori -</option>
												<option value="testi_baru">testi Baru</option>
												<option value="testi_akan_datang">testi Akan Datang</option>
												<option value="testi_berjalan">testi Berjalan</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label>Keterangan</label>
										<textarea id="summernote" type="text" name="keterangan" class="form-control" placeholder="Tulis di sini......" required>
											<?= $test->keterangan ?>
										</textarea>
									</div>

									<button type="submit" class="btn btn-primary waves-effect waves-light mt-3 col-12">Simpan</button>
								</form>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>