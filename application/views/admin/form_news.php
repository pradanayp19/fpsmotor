<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Edit Data Produk</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Data Produk</a></li>
						<li class="breadcrumb-item active">Edit Data Produk</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card m-b-30 mt-3">
						<div class="card-body">
							<?= form_open_multipart($form_action, ['method' => 'POST']) ?>
							<?= isset($input->id) ? form_hidden('id', $input->id) : '' ?>
							<div class="form-group">
								<label for="">Title</label>
								<?= form_input('title_news', $input->title_news, ['class' => 'form-control', 'id' => 'title_news', 'required' => true, 'autofocus' => true]) ?>
								<?= form_error('title_news') ?>
							</div>
							<div class="form-group">
								<label for="">Category</label>
								<?= form_input('category', $input->category, ['class' => 'form-control', 'id' => 'category', 'required' => true, 'autofocus' => true]) ?>
								<?= form_error('category') ?>
							</div>
							<div class="form-group">
								<label for="">Description</label>
								<?= form_textarea('description', $input->description, ['class' => 'form-control', 'id' => 'description', 'required' => true, 'autofocus' => true]) ?>
								<?= form_error('description') ?>
							</div>
							<div class="form-group">
								<label for="">Date News</label>
								<?= form_input('date_news', $input->date_news, ['class' => 'form-control', 'id' => 'date_news', 'required' => true, 'autofocus' => true]) ?>
								<?= form_error('date_news') ?>
							</div>
							<div class="form-group">
								<label for="">Gambar</label>
								<br>

								<?= form_upload('image_news') ?>
								<?php if ($this->session->flashdata('image_error')) : ?>
									<small class="form-text text-denger"><?= $this->session->flashdata('image_error') ?></small>
								<?php endif ?>
								<?php if ($input->image_news) : ?>
									<img src="<?= base_url("/images/news/$input->image_event") ?>" alt="" height="150">
								<?php endif ?>



							</div>
							<div class="form-group">
								<label for="">Admin</label>
								<?= form_input('admin', $input->admin, ['class' => 'form-control', 'id' => 'admin', 'required' => true]) ?>
								<?= form_error('admin') ?>
							</div>

							<button type="submit" class="btn btn-primary">Simpan</button>
							<?= form_close() ?>
						</div>
					</div>
				</div>
			</div>