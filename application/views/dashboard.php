<section id="hero" class="hero">
	<div id="hero-slider" class="hero-slider">

		<div class="slide bg-overlay">
			<div class="bg-section">
				<img src="assets/images/sliders/1.jpg" alt="Background" />
			</div>
			<div class="container vertical-center">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
						<div class="slide-content">
							<div class="slide-icon">
								<i class="icon icon-Settings"></i>
							</div>
							<div class="slide-heading"> Toko Sperpat terbaik untuk suku cadang anda! </div>
							<div class="slide-title">
								<h2>Say Hello To <span class="color-theme">FPS Motor</span></h2>
							</div>
							<div class="slide-desc"> FPS Motor is a business theme. Perfectly suited for Auto Mechanic, Car Repair Shops, Car Wash, Garages, Automobile Mechanicals, Mechanic Workshops, Auto Painting, Auto Centres. </div>
							<div class="slide-action">
								<a class="btn btn-primary" href="<?php echo base_url('Shop') ?>">Shop</a>
								<a class="btn btn-primary btn-white" href="<?php echo base_url('Showroom') ?>">Showroom</a>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div>


		<div class="slide bg-overlay">
			<div class="bg-section">
				<img src="assets/images/sliders/8.jpg" alt="Background" />
			</div>
			<div class="container vertical-center">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
						<div class="slide-content">
							<div class="slide-heading"> The Best product in market </div>
							<div class="slide-title">
								<h2>get all you need easily</h2>
							</div>
							<div class="slide-desc"> Car Shop is a business theme. Perfectly suited for Auto Mechanic, Car Repair Shops, Car Wash, Garages, Automobile Mechanicals, Mechanic Workshops, Auto Painting, Auto Centres. </div>
							<div class="slide-action">
								<a class="btn btn-primary" href="<?php echo base_url('Shop') ?>">Shop</a>
								<a class="btn btn-primary btn-white" href="<?php echo base_url('Showroom') ?>">Showroom</a>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div>


		<div class="slide bg-overlay">
			<div class="bg-section">
				<img src="assets/images/sliders/6.jpg" alt="Background" />
			</div>
			<div class="container vertical-center">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
						<div class="slide-content">
							<div class="slide-heading"> product start from $40 </div>
							<div class="slide-title">
								<h2>get by largest <span class="color-theme">brands</span></h2>
							</div>
							<div class="slide-desc"> Car Shop is a business theme. Perfectly suited for Auto Mechanic, Car Repair Shops, Car Wash, Garages, Automobile Mechanicals, Mechanic Workshops, Auto Painting, Auto Centres. </div>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</section>


<section id="featuredItems" class="shop">
	<div class="container">
		<div class="row product-boxes">

			<div class="col-xs-12 col-sm-4 col-md-4 product-box">
				<a href="<?php echo base_url('Shop') ?>">
					<div class="product-img">
						<img src="assets/images/shop/small/1.jpg" alt="Product" />
						<div class="product-hover">
							<div class="product-link">
								<h3>Shop</h3>
								<p>Best Metal</p>
							</div>
						</div>

					</div>

				</a>
			</div>


			<div class="col-xs-12 col-sm-4 col-md-4 product-box">
				<a href="<?php echo base_url('Showroom') ?>">
					<div class="product-img">
						<img src="assets/images/shop/small/3.jpg" alt="Product" />
						<div class="product-hover">
							<div class="product-link">
								<h3>Showroom</h3>
								<p>Best Prices</p>
							</div>
						</div>

					</div>

				</a>
			</div>


			<div class="col-xs-12 col-sm-4 col-md-4 product-box">
				<a href="<?php echo base_url('Event') ?>">
					<div class="product-img">
						<img src="assets/images/shop/small/3.jpg" alt="Product" />
						<div class="product-hover">
							<div class="product-link">
								<h3>Event</h3>
								<p>Best Quality</p>
							</div>
						</div>

					</div>

				</a>
			</div>

		</div>

	</div>

	<div class="container heading">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<p class="subheading">Katalog</p>
				<h2>Produk</h2>
			</div>

		</div>

	</div>
	<!-- BAGIAN PRODUK -->
	<div class="container">
		<div class="row product-carousel text-center">

			<?php foreach ($produk as $prod) : ?>

				<div class="product">
					<div class="product-img">
						<img src="<?= $prod->image ? base_url("/images/product/$prod->image") : base_url("/images/product/default.jpg") ?>" alt="" height="" class="card-img-top">
						<div class="product-hover">
							<div class="product-action">
								<a class="btn btn-primary" href="#">Detail Produk</a>
							</div>
						</div>


					</div>

					<div class="product-bio">

						<div class="prodcut-cat">
							<a href="<?= base_url('shop/detail_shop' . $prod->id) ?>"><?= $prod->category_title ?></a>
						</div>

						<div class="prodcut-title">
							<h3>
								<a href="<?= base_url('shop/detail_shop' . $prod->id) ?>"><?= $prod->product_title ?></a>
							</h3>
						</div>

						<div class="product-price">
							<span class="symbole">Rp. <?= number_format($prod->price), 0, ',', '.'  ?></span>
						</div>
					</div>
				</div>

			<?php endforeach; ?>

		</div>
	</div>

</section>
<!-- END BAGIAN PRODUk -->


<section id="clients" class="clients bg-gray">
	<div class="container heading">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<p class="subheading">Awesome</p>
				<h2>Our Brands</h2>
			</div>

		</div>

		<div class="row heading-desc">
			<div class="col-xs-12 col-sm-12 col-md-10">
				<p>Car Shop is a business theme. Perfectly suited for Auto Mechanic, Car Repair Shops, Car Wash, Garages, Automobile Mechanicals, Mechanic Workshops, Auto Painting, Auto Centres.</p>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-2">
				<a class="btn btn-primary btn-block" href="#">Check All Brands</a>
			</div>

		</div>

	</div>

	<div class="container">
		<div class="clients-bg">
			<div class="row">
				<div class="col-xs-12 colsm-12 col-md-12 client-carousel">

					<div class="client">
						<img src="assets/images/clients/1.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/2.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/3.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/4.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/5.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/6.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/4.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/5.png" alt="client">
					</div>


					<div class="client">
						<img src="assets/images/clients/6.png" alt="client">
					</div>

				</div>
			</div>

		</div>
	</div>

</section>

<section id="testimonials" class="testimonial  bg-gray">
	<div class="container heading">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<p class="subheading">People Say</p>
				<h2>Testimonials</h2>
			</div>

		</div>

		<div class="row heading-desc">
			<div class="col-xs-12 col-sm-12 col-md-10">
				<p>Car Shop is a business theme. Perfectly suited for Auto Mechanic, Car Repair Shops, Car Wash, Garages, Automobile Mechanicals, Mechanic Workshops, Auto Painting, Auto Centres.</p>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-2">
				<a class="btn btn-primary btn-block" href="#">Check All talks</a>
			</div>

		</div>

	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div id="testimonial-oc" class="testimonial-carousel">

					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/1.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>Begha</h6>
								<p>UI Designer, 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/2.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>Omar Elnagar</h6>
								<p>Civil Engineer , 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/3.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>Ahmed Abd Alhaleem</h6>
								<p>Graphic Designer, 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/2.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>ayman fikry</h6>
								<p>web developer, 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/3.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>mohamed fikry</h6>
								<p>web developer, 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/2.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>Begha</h6>
								<p>UI Designer, 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/2.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>Omar Elnagar</h6>
								<p>Civil Engineer , 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/3.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>Ahmed Abd Alhaleem</h6>
								<p>Graphic Designer, 7oroof Agency</p>
							</div>
						</div>
					</div>


					<div class="testimonial-item">
						<div class="testimonial-content">
							<i class="fa fa-quote-left"></i>
							<p>Lorem ipsum dolor sit consctetur adipiscing elit. Se lore quam, adipiscing in condimentum tristique vel, eleifend sed turpis.</p>
						</div>
						<div class="testimonial-meta">
							<img src="assets/images/testimonials/2.jpg" alt="author">
							<div class="testimonial-bio">
								<h6>ayman fikry</h6>
								<p>web developer, 7oroof Agency</p>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>

	</div>

</section>