<section id="page-title" class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<h1>EVENT</h1>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6">
				<ol class="breadcrumb text-right">
					<li>
						<a href="index.html">Home</a>
					</li>
					<li class="active">blog grid</li>
				</ol>
			</div>

		</div>

	</div>

</section>


<section id="blog" class="blog blog-grid">
	<div class="container">
		<div class="row">
			<?php foreach ($eventfps as $eve) : ?>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="entry mb-30">
						<div class="entry-img">
							<img src="<?php echo $eve->image_event ? base_url("/images/event/$eve->image_event") : base_url("images/event/default.jpg")  ?>" alt="" width="4000px" height="100px" />

						</div>

						<div class="entry-content">
							<div class="entry-format">
								<i class="fa fa-image"></i>
							</div>
							<div class="entry-title">
								<h3>
									<a href="<?= base_url('shop/detail_shop' . $eve->id_event) ?>"><?= $eve->title_event ?></a></a>
								</h3>
							</div>

							<ul class="entry-meta list-inline clearfix">
								<li class="entry-date">On: <span><?= $eve->date_event ?></span>
								</li>
								<li class="entry-author">By:
									<span><?= $eve->admin ?></span>
								</li>
								<li class="entry-num-comments">Comments:
									<a href="#">0</a>
								</li>
							</ul>

							<div class="entry-snippet">
								<p><?= $eve->description ?></p>
							</div>

						</div>

					</div>

				</div>

			<?php endforeach ?>
		</div>

		<nav aria-label="Page navigation example">
			<?= $pagination ?>
		</nav>


	</div>

</section>