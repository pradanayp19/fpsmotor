<section id="page-title" class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <h1>DETAIL EVENT</h1>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6">
                <ol class="breadcrumb text-right">
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li class="active">blog</li>
                </ol>
            </div>

        </div>

    </div>

</section>


<section id="blog" class="blog blog-single">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9">
                <div class="entry">
                    <div class="entry-img">
                        <a href="#">
                            <img src="<?= base_url() ?>assets/images/blog/standard/1.jpg" alt="Entry Title">
                        </a>
                    </div>

                    <div class="entry-content">
                        <div class="entry-format">
                            <i class="fa fa-image"></i>
                        </div>
                        <div class="entry-title">
                            <h3>
                                <a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
                            </h3>
                        </div>

                        <ul class="entry-meta list-inline clearfix">
                            <li class="entry-date">On: <span>Feb 12, 2015</span></li>
                            <li class="entry-author">By:
                                <a href="#">Begha</a>
                            </li>
                            <li class="entry-num-comments">Comments:
                                <a href="#">45</a>
                            </li>
                            <li class="entry-category">Category :
                                <a href="#">Construction</a>
                                ,
                                <a href="#">Tips and Tricks</a>
                            </li>
                            <li class="entry-views">View : 25445</li>
                        </ul>

                        <div class="entry-snippet">
                            <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. </p>
                            <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. </p>
                            <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. </p>
                            <p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. </p>
                        </div>


                        <div class="entry-tags">
                            <div class="widget widget-tags">
                                <h6>tags</h6>
                                <a href="#">responsive</a>
                                <a href="#">modern</a>
                                <a href="#">corporate</a>
                                <a href="#">business</a>
                                <a href="#">fresh</a>
                                <a href="#">awesome</a>
                                <a href="#">business</a>
                                <a href="#">fresh</a>
                                <a href="#">corporate</a>
                            </div>

                        </div>

                        <div class="entry-share">
                            <div class="share-title pull-left pull-none-xs">
                                <h6>share this article: </h6>
                            </div>
                            <div class="share-links text-right">
                                <a class="share-facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="share-twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="share-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="share-pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                                <a class="share-dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                <a class="share-linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="entry-author-box">
                    <div class="entry-author-img">
                        <img src="<?= base_url() ?>assets/images/blog/author/1.jpg" alt="Entry Author">
                    </div>

                    <div class="entry-author-bio">
                        <h6>Author</h6>
                        <p>Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. </p>
                    </div>

                </div>

                <div class="entry-comments">
                    <div class="comments-heading">
                        <h6><span>3 </span>Comments</h6>
                    </div>
                    <div class="comments-body">
                        <ul class="comments-list">
                            <li class="comment-body">
                                <div class="comment-content">
                                    <div class="avatar">
                                        <img src="<?= base_url() ?>assets/images/blog/author/2.jpg" alt="avatar" />
                                    </div>
                                    <div class="comment">
                                        <div class="comment-meta">
                                            <h6>Mahmoud Baghagho</h6>
                                            <div class="date">Dec 17, 2014</div>
                                            <div class="comment-reply">
                                                <a class="reply" href="#">Reply</a>
                                            </div>
                                        </div>
                                        <p>Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor.</p>
                                    </div>
                                </div>
                            </li>

                            <li class="comment-body">
                                <div class="comment-content">
                                    <div class="avatar">
                                        <img src="<?= base_url() ?>assets/images/blog/author/3.jpg" alt="avatar" />
                                    </div>
                                    <div class="comment">
                                        <div class="comment-meta">
                                            <h6>Mostafa Amin</h6>
                                            <div class="date">Dec 17, 2014</div>
                                            <div class="comment-reply">
                                                <a class="reply" href="#">Reply</a>
                                            </div>
                                        </div>
                                        <p>Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor.</p>
                                    </div>
                                </div>
                                <ul class="comment-children">
                                    <li class="comment-body">
                                        <div class="comment-content">
                                            <div class="avatar">
                                                <img src="<?= base_url() ?>assets/images/blog/author/2.jpg" alt="avatar" />
                                            </div>
                                            <div class="comment">
                                                <div class="comment-meta">
                                                    <h6>Mahmoud Baghagho</h6>
                                                    <div class="date">Dec 17, 2014</div>
                                                    <div class="comment-reply">
                                                        <a class="reply" href="#">Reply</a>
                                                    </div>
                                                </div>
                                                <p>Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor.</p>
                                            </div>
                                        </div>
                                    </li>

                                </ul>

                            </li>

                            <li class="comment-body">
                                <div class="comment-content">
                                    <div class="avatar">
                                        <img src="<?= base_url() ?>assets/images/blog/author/3.jpg" alt="avatar" />
                                    </div>
                                    <div class="comment">
                                        <div class="comment-meta">
                                            <h6>Ahmed Hassan</h6>
                                            <div class="date">Dec 17, 2014</div>
                                            <div class="comment-reply">
                                                <a class="reply" href="#">Reply</a>
                                            </div>
                                        </div>
                                        <p>Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor.</p>
                                    </div>
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>

                <div class="entry-comments-forms">
                    <div class="comment-form-heading">
                        <h6>Leave Comment</h6>
                    </div>
                    <div class="comment-form-body">
                        <form id="entry-comments-form" class="mb-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="Comment-name" id="name" placeholder="Your Name" required />
                                    <input type="email" class="form-control" name="Comment-email" id="email" placeholder="Your Email" required />
                                    <button type="submit" id="submit-message" class="btn btn-primary btn-block">Add Comment</button>
                                </div>
                                <div class="col-md-6">
                                    <textarea name="Comment-message" id="Comment" rows="4" placeholder="Comment" required></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 sidebar">

                <div class="widget widget-categories">
                    <div class="widget-title">
                        <h5>categories</h5>
                    </div>
                    <div class="widget-content">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#"><i class="fa fa-angle-double-right"></i>Branding<span>(5)</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-angle-double-right"></i>Typography<span>(77)</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-angle-double-right"></i>UI Design<span>(6)</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-angle-double-right"></i>Wordpress<span>(11)</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-angle-double-right"></i>Development<span>(54)</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-angle-double-right"></i>Photography<span>(22)</span></a>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="widget widget-recent">
                    <div class="widget-title">
                        <h5>recent posts</h5>
                    </div>
                    <div class="widget-content">
                        <div class="entry">
                            <img src="<?= base_url() ?>assets/images/sidebar/1.jpg" alt="title" />
                            <div class="entry-desc">
                                <div class="entry-title">
                                    <a href="#">home renovations with modern style</a>
                                </div>
                                <div class="entry-meta">
                                    <div>on: <span>Dec 10, 2014</span></div>
                                    <div>by:
                                        <a href="#"> Begha</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="entry">
                            <img src="<?= base_url() ?>assets/images/sidebar/2.jpg" alt="title" />
                            <div class="entry-desc">
                                <div class="entry-title">
                                    <a href="#">make your home your paradise</a>
                                </div>
                                <div class="entry-meta">
                                    <div>on: <span>Dec 10, 2014</span></div>
                                    <div>by:
                                        <a href="#"> Begha</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="entry">
                            <img src="<?= base_url() ?>assets/images/sidebar/3.jpg" alt="title" />
                            <div class="entry-desc">
                                <div class="entry-title">
                                    <a href="#">How Important is Light in the Living Room?</a>
                                </div>
                                <div class="entry-meta">
                                    <div>on: <span>Dec 10, 2014</span></div>
                                    <div>by:
                                        <a href="#"> Begha</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>


                <div class="widget widget-tags">
                    <div class="widget-title">
                        <h5>tag clouds</h5>
                    </div>
                    <div class="widget-content">
                        <a href="#">responsive</a>
                        <a href="#">modern</a>
                        <a href="#">blog</a>
                        <a href="#">corporate</a>
                        <a href="#">mount</a>
                        <a href="#">business</a>
                        <a href="#">awesome</a>
                        <a href="#">marketing</a>
                        <a href="#">fresh</a>
                    </div>
                </div>


                <div class="widget widget-flickr">
                    <div class="widget-title">
                        <h5>Flickr Stream</h5>
                    </div>
                    <div class="widget-content">
                        <div id="flickr-feed"></div>
                        <a class="flickr-more" href="#">View Stream On Flickr</a>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>