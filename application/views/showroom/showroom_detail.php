<section id="page-title" class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <h1>Shop Single Product</h1>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6">
                <ol class="breadcrumb text-right">
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li class="active">shop</li>
                </ol>
            </div>

        </div>

    </div>

</section>


<section id="shopgrid" class="shop shop-single">
    <div class="container shop-content">

        <div class="row">
            <?php foreach ($showroom as $show) : ?>
                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="product-images">
                        <div class="product-img-slider">
                            <img src="<?php echo base_url() . '/upload/' . $show->gambar_psr ?>" alt="product images" class="img-thumbnail" width="300px">

                        </div>
                        <div class="product-img-nav">
                            <img src="<?php echo base_url() . '/upload/' . $show->gambar_psr ?>" alt="product images" class="img-thumbnail" width="300px">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7">
                    <div class="product-title text-center-xs">
                        <h3><?= $show->nama ?></h3>
                    </div>

                    <div class="product-meta mb-60">
                        <div class="product-price pull-left pull-none-xs">
                            <p>Rp. <?= number_format($show->harga), 0, ',', '.'  ?></p>
                        </div>
                    </div>

                    <div class="product-desc text-center-xs">
                        <p class="mb-">Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio.</p>
                    </div>

                    <hr class="mt-30 mb-30">
                    <div class="product-details text-center-xs">
                        <h5>Other Details :</h5>
                        <ul class="list-unstyled">

                            <li>Code : <?= $show->kode_psr ?><span></span></li>
                            <li>Product : <?= $show->nama ?><span></span></li>
                            <li>Kategori : <?= $show->kategori_psr ?><span></span></li>
                        </ul>
                    </div>
                    <div class="product-quantity pull-left pull-none-xs">
                        <span class="qua">
                            <h3> Stok Tersedia: <?= $show->stok ?> Items</h3>
                        </span>
                    </div>

                    <div class="product-cta text-right text-center-xs">
                        <a class="btn btn-primary" href="#">add to cart</a>
                        <a class="btn btn-primary bordered" href="#">wishlist</a>
                        <a class="btn btn-primary bordered" href="#">compare</a>
                    </div>

                    <hr class="mt-30 mb-30">
                    <div class="product-share  text-center-xs">
                        <h5 class="share-title">share product: </h5>
                        <a class="share-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="share-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="share-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                        <a class="share-pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                        <a class="share-dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                    </div>

                </div>
        </div>
    <?php endforeach ?>
    </div>

</section>