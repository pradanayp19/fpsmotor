<!DOCTYPE html>
<html dir="ltr" lang="en-US">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="author" content="zytheme" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="construction html5 template">
	<link href="<?= base_url() ?>assets/images/logofps/Logofps.png" rel="icon">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i%7CRaleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7CUbuntu:300,300i,400,400i,500,500i,700,700i" rel='stylesheet' type='text/css'>

	<link href="<?= base_url() ?>assets/css/external.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">

	<!-- Bootstrap core CSS -->
	<!-- <link href=" <?= base_url('/') ?>assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->

	<!-- fontawesome css -->
	<link rel="stylesheet" href=" <?= base_url('/') ?>assets/libs/fontawesome/css/all.min.css">


	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
		var Tawk_API = Tawk_API || {},
			Tawk_LoadStart = new Date();
		(function() {
			var s1 = document.createElement("script"),
				s0 = document.getElementsByTagName("script")[0];
			s1.async = true;
			s1.src = 'https://embed.tawk.to/62e2dcd254f06e12d88bd2c5/1g932t5se';
			s1.charset = 'UTF-8';
			s1.setAttribute('crossorigin', '*');
			s0.parentNode.insertBefore(s1, s0);
		})();
	</script>
	<!--End of Tawk.to Script-->

	<!--[if lt IE 9]>
      <script src="<?= base_url() ?>assets/js/html5shiv.js"></script>
      <script src="<?= base_url() ?>assets/js/respond.min.js"></script>
  <![endif]-->

	<title>FPS Motor</title>
</head>

<?php $this->load->view('template/navbar') ?>

<?php $this->load->view($page); ?>

<?php $this->load->view('template/footer') ?>